namespace PeerTube {
	public class Account : GLib.Object {
		internal Account (GJson.Node node) {
			this.avatar = new Avatar (node["avatar"]);
			TimeVal tv = TimeVal();
			tv.from_iso8601 (node["createdAt"].as_string());
			this.created_at = new DateTime.from_timeval_utc (tv);
			tv = TimeVal();
			tv.from_iso8601 (node["updatedAt"].as_string());
			this.updated_at = new DateTime.from_timeval_utc (tv);
			this.display_name = node["displayName"].as_string();
			this.followers_count = node["followersCount"].as_integer();
			this.following_count = node["followingCount"].as_integer();
			this.name = node["name"].as_string();
			this.host = node["host"].as_string();
			this.uuid = node["uuid"].as_string();
			this.url = node["url"].as_string();
			this.id = node["id"].as_integer();
		}
		
		public PeerTube.Avatar avatar { get; private set; }
		
		public DateTime created_at { get; private set; }
		
		public DateTime updated_at { get; private set; }
		
		public string display_name { get; private set; }
		
		public int64 following_count { get; private set; }
		
		public int64 followers_count { get; private set; }
		
		public string name { get; private set; }
		
		public string host { get; private set; }
		
		public string uuid { get; private set; }
		
		public string url { get; private set; }
		
		public int64 id { get; private set; }
	}
}

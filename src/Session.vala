namespace PeerTube {
	public errordomain SessionError {
		NULL,
		FAILED,
		AUTH_FAILED,
		NOT_CONNECTED
	}
	
	public class Session : GLib.Object {
		Soup.Session session;
		string access_token;
		string token_type;
		
		construct {
			this.session = new Soup.Session();
		}
		
		public string server { get; private set; }
		
		public PeerTube.User me() throws GLib.Error {
			if (this.server == null)
				throw new SessionError.NOT_CONNECTED ("not connected");
			var message = new Soup.Message ("GET", this.server + "/api/v1/users/me");
			message.request_headers.append ("Accept", "application/json");
			message.request_headers.append ("Authorization", "Bearer " + this.access_token);
			this.session.send_message (message);
			if (message.status_code != 200)
				throw new SessionError.FAILED ("failed : %s".printf (message.reason_phrase));
			var object = GJson.Node.parse ((string)message.response_body.data);
			return new User (object);
		}
		
		public bool connect (string server, string mail_or_username, string password) throws GLib.Error {
			var message = new Soup.Message ("GET", server + "/api/v1/oauth-clients/local");
			this.session.send_message (message);
			if (message.status_code != 200)
				throw new SessionError.FAILED ("Connection failed.");
			var object = GJson.Object.parse ((string)message.response_body.data);
			message = Soup.Form.request_new ("POST", server + "/api/v1/users/token",
				"client_id", object["client_id"].as_string(),
				"client_secret", object["client_secret"].as_string(),
				"response_type", "code",
				"grant_type", "password",
				"scope", "upload",
				"username", mail_or_username,
				"password", password);
			this.session.send_message (message);
			if (message.status_code != 200)
				throw new SessionError.AUTH_FAILED ("Authentication failed.");
			object = GJson.Object.parse ((string)message.response_body.data);
			this.access_token = object["access_token"].as_string();
			this.token_type = object["token_type"].as_string();
			this.server = server;
			return true;
		}
		
		public PeerTube.Avatar change_avatar (uint8[] avatar) {
			var fd = new Soup.Multipart (Soup.FORM_MIME_TYPE_MULTIPART);
			var buffer = new Soup.Buffer.take (avatar);
			fd.append_form_file ("avatarfile", "avatar.png", "image/png", buffer);
			var message = Soup.Form.request_new_from_multipart (this.server + "/api/v1/users/me/avatar/pick", fd);
			message.request_headers.append ("Accept", "application/json, text/plain, */*");
			message.request_headers.append ("Authorization", "Bearer " + this.access_token);
			this.session.send_message (message);
			if (message.status_code != 200)
				throw new SessionError.FAILED ("failed : %s".printf (message.reason_phrase));
			GJson.Object object = GJson.Object.parse ((string)message.response_body.data);
			return new Avatar (object["avatar"]);
		}
		
		public PeerTube.Avatar change_avatar_file (string filename) throws GLib.Error {
			uint8[] data;
			FileUtils.get_data (filename, out data);
			return change_avatar (data);
		}
		
		public Gee.List<PeerTube.Video> get_videos (int start = 0, int count = 25) throws GLib.Error {
			if (this.server == null)
				throw new SessionError.NOT_CONNECTED ("not connected");
			var message = new Soup.Message ("GET", this.server + "/api/v1/videos/?start=%d&count=%d".printf (start, count));
			message.request_headers.append ("Accept", "application/json");
			message.request_headers.append ("Authorization", "Bearer " + this.access_token);
			this.session.send_message (message);
			if (message.status_code != 200)
				throw new SessionError.FAILED ("failed : %s".printf (message.reason_phrase));
			var object = GJson.Object.parse ((string)message.response_body.data);
			var list = new Gee.ArrayList<Video>();
			object["data"].foreach (node => {
				list.add (new Video (node));
				return true;
			});
			return list;
		}
		
		public Gee.List<PeerTube.Video> search_videos (string query, int start = 0, int count = 25) throws GLib.Error {
			if (this.server == null)
				throw new SessionError.NOT_CONNECTED ("not connected");
			var message = new Soup.Message ("GET", this.server + "/api/v1/videos/search?start=%d&count=%d&search=%s".printf (start, count, query));
			message.request_headers.append ("Accept", "application/json");
			message.request_headers.append ("Authorization", "Bearer " + this.access_token);
			this.session.send_message (message);
			if (message.status_code != 200)
				throw new SessionError.FAILED ("failed : %s".printf (message.reason_phrase));
			var object = GJson.Object.parse ((string)message.response_body.data);
			var list = new Gee.ArrayList<Video>();
			object["data"].foreach (node => {
				list.add (new Video (node));
				return true;
			});
			return list;
		}
	}
}

PeerTube-GLib
=============

GLib wrapper to PeerTube API.

### Requirements
 * [gjson-1.0](https://github.com/BZHDeveloper/vul)
 * [libsoup-2.4](https://git.gnome.org/browse/libsoup/)
 * [meson](http://mesonbuild.com/Getting-meson.html)
 * [ninja](https://ninja-build.org)

namespace PeerTube {
	public enum VideoCategory {
		UNKNOWN,
		MUSIC,
		FILMS,
		VEHICLES,
		ART,
		SPORTS,
		TRAVELS,
		GAMING,
		PEOPLE,
		COMEDY,
		ENTERTAINMENT,
		NEWS,
		HOW_TO,
		EDUCATION,
		ACTIVISM,
		SCIENCE_TECHNOLOGY,
		ANIMALS,
		KIDS,
		FOOD
	}
	
	public enum VideoLicence {
		UNKNOWN,
		ATTRIBUTION,
		ATTRIBUTION_SHARE_ALIKE,
		ATTRIBUTION_NO_DERIVATIVES,
		ATTRIBUTION_NON_COMMERCIAL,
		ATTRIBUTION_NON_COMMERCIAL_SHARE_ALIKE,
		ATTRIBUTION_NON_COMMERCIAL_NO_DERIVATIVES
	}
	
	public enum VideoLanguage {
		UNKNOWN,
		ENGLISH,
		SPANISH,
		MANDARIN,
		HINDI,
		ARABIC,
		PORTUGUESE,
		BENGALI,
		RUSSIAN,
		JAPANESE,
		PUNJABI,
		GERMAN,
		KOREAN,
		FRENCH,
		ITALIAN
	}
	
	public class Video : GLib.Object {
		public int64 id { get; private set; }
		
		public string uuid { get; private set; }
		
		public string account_name { get; private set; }
		
		public DateTime created_at { get; private set; }
		
		public DateTime updated_at { get; private set; }
		
		public PeerTube.VideoCategory category { get; private set; }
		
		public PeerTube.VideoLicence licence { get; private set; }
		
		public PeerTube.VideoLanguage language { get; private set; }
		
		public string description { get; private set; }
		
		public int64 duration { get; private set; }
		
		public bool is_local { get; private set; }
		
		public string name { get; private set; }
		
		public string server_host { get; private set; }
		
		public string thumbnail_path { get; private set; }
		
		public string preview_path { get; private set; }
		
		public string embed_path { get; private set; }
		
		public int64 views { get; private set; }
		
		public int64 likes { get; private set; }
		
		public int64 dislikes { get; private set; }
		
		public bool nsfw { get; private set; }
		
		internal Video (GJson.Node node) {
			this.id = node["id"].as_integer();
			this.uuid = node["uuid"].as_string();
			this.account_name = node["accountName"].as_string();
			TimeVal tv = TimeVal();
			tv.from_iso8601 (node["createdAt"].as_string());
			this.created_at = new DateTime.from_timeval_utc (tv);
			tv = TimeVal();
			tv.from_iso8601 (node["updatedAt"].as_string());
			this.updated_at = new DateTime.from_timeval_utc (tv);
			this.category = (VideoCategory)node["category"].as_integer();
			this.licence = (VideoLicence)node["licence"].as_integer();
			this.language = (VideoLanguage)node["language"].as_integer();
			this.description = node["description"].as_string();
			this.duration = node["duration"].as_integer();
			this.is_local = node["isLocal"].as_boolean();
			this.name = node["name"].as_string();
			this.server_host = node["serverHost"].as_string();
			this.thumbnail_path = node["thumbnailPath"].as_string();
			this.preview_path = node["previewPath"].as_string();
			this.embed_path = node["embedPath"].as_string();
			this.views = node["views"].as_integer();
			this.likes = node["likes"].as_integer();
			this.dislikes = node["dislikes"].as_integer();
			this.nsfw = node["nsfw"].as_boolean();
		}
	}
}

namespace PeerTube {
	public enum UserRole {
		ADMINISTRATOR,
		MODERATOR,
		USER
	}
	
	public class User : GLib.Object {
		internal User (GJson.Node node) {
			this.id = node["id"].as_integer();
			this.username = node["username"].as_string();
			this.email = node["email"].as_string();
			this.display_nsfw = node["displayNSFW"].as_boolean();
			this.auto_play_video = node["autoPlayVideo"].as_boolean();
			this.role = (UserRole)node["role"].as_integer();
			this.video_quota = node["videoQuota"].as_integer();
			TimeVal tv = TimeVal();
			tv.from_iso8601 (node["createdAt"].as_string());
			this.created_at = new DateTime.from_timeval_utc (tv);
			this.account = new Account (node["account"]);
		}
		
		public int64 id { get; private set; }
		
		public string username { get; private set; }
		
		public string email { get; private set; }
		
		public bool display_nsfw { get; private set; }
		
		public bool auto_play_video { get; private set; }
		
		public PeerTube.UserRole role { get; private set; }
		
		public int64 video_quota { get; private set; }
		
		public DateTime created_at { get; private set; }
		
		public PeerTube.Account account { get; private set; }
	}
}

/*
	"id" : 35,
	"username" : "bzhdeveloper",
	"email" : "inizan.yannick@gmail.com",
	"displayNSFW" : false,
	"autoPlayVideo" : true,
	"role" : 2,
	"roleLabel" : "User",
	"videoQuota" : 5368709120,
	"createdAt" : "2018-02-25T04:31:35.314Z"
*/

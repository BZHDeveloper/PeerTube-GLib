namespace PeerTube {
	public class Avatar : GLib.Object {
		internal Avatar (GJson.Node node) {
			this.path = node["path"].as_string();
			TimeVal tv = TimeVal();
			tv.from_iso8601 (node["createdAt"].as_string());
			this.created_at = new DateTime.from_timeval_utc (tv);
			tv = TimeVal();
			tv.from_iso8601 (node["updatedAt"].as_string());
			this.updated_at = new DateTime.from_timeval_utc (tv);
		}
		
		public string path { get; private set; }
		
		public DateTime created_at { get; private set; }
		
		public DateTime updated_at { get; private set; }
	}
}
